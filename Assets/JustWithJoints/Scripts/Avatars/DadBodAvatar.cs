using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace JustWithJoints.Avatars {
    public class DadBodAvatar : MonoBehaviour {
        public MediaPipePoseTranslator poseTranslator; //TODO - make parent class of PoseTranslator for different models. 
        public IdlePoseContainer idlePoseContainer;
        public GameObject root;
        public GameObject hips;
        public GameObject rightUpLeg;
        public GameObject rightLeg;
        public GameObject rightFoot;
        public GameObject rightToeBase;
        public GameObject leftUpLeg;
        public GameObject leftLeg;
        public GameObject leftFoot;
        public GameObject leftToeBase;
        public GameObject spine;
        public GameObject spine1;
        public GameObject spine2;
        public GameObject rightShoulder;
        public GameObject rightArm;
        public GameObject rightForeArm;
        public GameObject rightHand;
        public GameObject leftShoulder;
        public GameObject leftArm;
        public GameObject leftForeArm;
        public GameObject leftHand;
        public GameObject neck;
        public GameObject head;
        private float boneSmoothAmount = 10;
        private float highConfidenceSmoothAmount = 11.5f;
        private float midConfidenceSmoothAmount = 7.5f;
        private float lowConfidenceSmoothAmount = 3.6f;
        private float targetSmoothAmount = 5;
        private float currentSmoothAmount = 5; //lerping between smooth amountsd
        private int numFramesConfident;
        private int goalFramesConfident = 150;

        public bool EnableRetargetting = true;
        public bool RetargetTranslation = true;

        List<GameObject> joints = new List<GameObject> ();
        List<GameObject> bones_ = new List<GameObject> ();

        /*public*/
        Vector3[] correctionRightEulers = new Vector3[13] {
            new Vector3 (0, 0, 0), //hips
            new Vector3 (0, 0, 0), //rightUpLeg
            new Vector3 (0, 0, 0), //rightLeg
            new Vector3 (0, 0, 0), //rightUpleg
            new Vector3 (0, 0, 0), //leftLeg
            new Vector3 (0, 0, 0), //spine
            new Vector3 (0, 0, 0), //rightShoulder
            new Vector3 (0, 0, 0), //rightArm
            new Vector3 (0, 0, 0), //rightForeArm
            new Vector3 (0, 0, 0), //leftShoulder
            new Vector3 (0, 0, 0), //leftArm
            new Vector3 (0, 0, 0), //leftForeArm
            new Vector3 (0, 0, 0), //neck
        };

        void Start () {
            bones_.AddRange (new GameObject[] {
                hips,
                rightUpLeg,
                rightLeg,
                leftUpLeg,
                leftLeg,
                spine,
                rightShoulder,
                rightArm,
                rightForeArm,
                leftShoulder,
                leftArm,
                leftForeArm,
                neck,
            });
        }

        private float getSmoothAmountFromConfidence (MediaPipePoseTranslator poseTranslator) {
            if (!poseTranslator.lostTracking) {
                numFramesConfident++;
            } else {
                numFramesConfident = 0;
            }
            if (numFramesConfident > goalFramesConfident) {
                if (numFramesConfident > (goalFramesConfident * 3)) {
                    boneSmoothAmount = midConfidenceSmoothAmount;
                } else {
                    Debug.Log ("high confidence!");
                    boneSmoothAmount = highConfidenceSmoothAmount;
                }
            } else {
                boneSmoothAmount = lowConfidenceSmoothAmount;
            }
            return boneSmoothAmount;
        }

        void LateUpdate () {
            // Get pose
            Core.Pose pose = null;
            Core.Pose idlePose = null;

            if (poseTranslator != null) {
                pose = poseTranslator.getCurrentPose ();
                boneSmoothAmount = getSmoothAmountFromConfidence (poseTranslator);

            } else {
                Debug.Log ("no pose translator found!");
            }

            if (idlePoseContainer != null) {
                idlePose = idlePoseContainer.getCurrentPose ();
            } else {
                Debug.Log ("no idle pose container!");
            }

            // Retarget
            if (pose != null) {
                // Retarget positions
                if (RetargetTranslation) {
                    bones_[0].transform.position = (pose.Positions[2] + pose.Positions[3]) * 0.5f + gameObject.transform.position;
                }

                if (EnableRetargetting) {
                    // Retarget rotations
                    for (int i = 0; i < bones_.Count; i++) {
                        int boneIndex = i;
                        if (i == (int) Core.BoneType.Trans) {
                            // Use spine as root of avatar
                            boneIndex = (int) Core.BoneType.Spine;
                        }
                        if (i == (int) Core.BoneType.Spine) {
                            // Skip spine
                            continue;
                        }
                        if (i == (int) Core.BoneType.RightShoulder || i == (int) Core.BoneType.LeftShoulder) {
                            // Skip shoulders because bone hierarchy is not same as Core.Pose
                            // hmm...
                            //continue;
                        }

                        Quaternion poseRotation = pose.Rotations[boneIndex];

                        if (!poseTranslator.lostTracking) {
                            if (i < 5) {
                                //LOWER BODY - HIPS AND BELOW
                                if (poseTranslator.torsoOnlyMode) {
                                    poseRotation = idlePose.Rotations[boneIndex];
                                }
                            } else {
                                // TODO - handle legs only mode. 
                            }
                        } else {
                            poseRotation = idlePose.Rotations[boneIndex];
                        }

                        //--- calculate with parent transform. 
                        Quaternion newBoneRotation = root.transform.rotation * poseRotation * Quaternion.Euler (correctionRightEulers[i]);
                        //--- smooth
                        bones_[i].transform.rotation = Quaternion.Lerp (bones_[i].transform.rotation, newBoneRotation, Time.deltaTime * boneSmoothAmount);
                        if (!poseTranslator.lostTracking) {
                            updateIdleLegOrientation (bones_[5].transform.rotation, idlePoseContainer);
                        }
                    }
                }
            }
            hips.transform.localPosition = adjustPositionFromFeet ();
        }

        Vector3 adjustPositionFromFeet () {
            Vector3 betweenFeet = Vector3.Lerp (rightToeBase.transform.position, leftToeBase.transform.position, 0.5f);
            float minY = rightToeBase.transform.position.y;
            minY = leftToeBase.transform.position.y < minY ? leftToeBase.transform.position.y : minY;
            betweenFeet.y = minY;
            //TODO triangulate diff in Y between leftToeBase and rightToeBase, lerp between dominant foot.  
            //betweenFeet = leftToeBase.transform.position; //looks good for one foot. put some smooething in. 
            Vector3 adjustedPosition = new Vector3 (0, -1, 0) - betweenFeet + transform.position;
            return adjustedPosition;
        }

        void updateIdleLegOrientation (Quaternion rotation, IdlePoseContainer idlePoseContainer) {
            //attempting hips.
            Vector3 eulerRotation = rotation.eulerAngles;
            //TODO - put lerp here. 
            idlePoseContainer.transform.rotation = Quaternion.Euler (0, eulerRotation.y + 90, 0);
        }

    }
}