using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace JustWithJoints.Avatars {
    public class JinxFullRigAvatar : MonoBehaviour {
        //has no working skeleton

        public MediaPipePoseTranslator poseTranslator; //TODO - make parent class of PoseTranslator for different models. 
        public GameObject hips;
        public GameObject rightUpLeg;
        public GameObject rightLeg;
        public GameObject rightFoot;
        public GameObject rightToeBase;
        public GameObject leftUpLeg;
        public GameObject leftLeg;
        public GameObject leftFoot;
        public GameObject leftToeBase;
        public GameObject spine;
        public GameObject spine1;
        public GameObject spine2;
        public GameObject rightShoulder;
        public GameObject rightArm;
        public GameObject rightForeArm;
        public GameObject rightHand;
        public GameObject leftShoulder;
        public GameObject leftArm;
        public GameObject leftForeArm;
        public GameObject leftHand;
        public GameObject neck;
        public GameObject head;

        public bool EnableRetargetting = true;
        public bool RetargetTranslation = true;

        List<GameObject> joints = new List<GameObject> ();
        List<GameObject> bones_ = new List<GameObject> ();

        /*public*/
        Vector3[] correctionRightEulers = new Vector3[13] {
            new Vector3 (0, 0, 0), //hips
            new Vector3 (0, 0, 0), //rightUpLeg
            new Vector3 (0, 0, 0), //rightLeg
            new Vector3 (0, 0, 0), //rightUpleg
            new Vector3 (0, 0, 0), //leftLeg
            new Vector3 (0, 0, 0), //spine
            new Vector3 (0, 0, 0), //rightShoulder
            new Vector3 (0, 0, 0), //rightArm
            new Vector3 (0, 0, 0), //rightForeArm
            new Vector3 (0, 0, 0), //leftShoulder
            new Vector3 (0, 0, 0), //leftArm
            new Vector3 (0, 0, 0), //leftForeArm
            new Vector3 (0, 0, 0), //neck
        };

        void Start () {
            bones_.AddRange (new GameObject[] {
                hips,
                rightUpLeg,
                rightLeg,
                leftUpLeg,
                leftLeg,
                spine,
                rightShoulder,
                rightArm,
                rightForeArm,
                leftShoulder,
                leftArm,
                leftForeArm,
                neck,
            });
        }

        void LateUpdate () {
            // Get pose
            Core.Pose pose = null;

            // if (MotionProvider) {
            //     var component = MotionProvider.GetComponent<CMUMotionPlayer> ();
            //     if (component) {
            //         pose = component.GetCurrentPose ();
            //     }
            // }

            if (poseTranslator != null) {
                pose = poseTranslator.getCurrentPose ();
            } else {
                Debug.Log ("no pose translator found!");
            }

            // Retarget
            if (pose != null) {
                // Retarget positions
                if (RetargetTranslation) {
                    bones_[0].transform.position = (pose.Positions[2] + pose.Positions[3]) * 0.5f + gameObject.transform.position;
                }

                if (EnableRetargetting) {
                    // Retarget rotations
                    for (int i = 0; i < bones_.Count; i++) {
                        int boneIndex = i;
                        if (i == (int) Core.BoneType.Trans) {
                            // Use spine as root of avatar
                            boneIndex = (int) Core.BoneType.Spine;
                        }
                        if (i == (int) Core.BoneType.Spine) {
                            // Skip spine
                            continue;
                        }
                        if (i == (int) Core.BoneType.RightShoulder || i == (int) Core.BoneType.LeftShoulder) {
                            // Skip shoulders because bone hierarchy is not same as Core.Pose
                            continue;
                        }
                        bones_[i].transform.rotation = pose.Rotations[boneIndex] * Quaternion.Euler (correctionRightEulers[i]);
                        //bones_[i].transform.rotation = pose.Rotations[boneIndex];
                    }
                }
            }
        }
    }
}