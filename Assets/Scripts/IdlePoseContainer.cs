using System.Collections.Generic;
using JustWithJoints.Avatars;
using UnityEngine;

public class IdlePoseContainer : MonoBehaviour {
    public DadBodAvatar dadbodAvatar;

    public GameObject root;
    public GameObject hips;
    public GameObject leftUpLeg;
    public GameObject leftLeg;
    public GameObject leftFoot;
    public GameObject leftToeBase;
    public GameObject rightUpLeg;
    public GameObject rightLeg;
    public GameObject rightFoot;
    public GameObject rightToeBase;
    public GameObject spine;
    public GameObject spine1;
    public GameObject spine2;
    public GameObject leftShoulder;
    public GameObject leftArm;
    public GameObject leftForeArm;
    public GameObject leftHand;
    public GameObject rightShoulder;
    public GameObject rightArm;
    public GameObject rightForeArm;
    public GameObject rightHand;
    public GameObject neck;
    public GameObject head;

    private string[] boneNames = {
        "rtibia", //right foot  ( shin ) 
        "rfemur", //rightLeg ( hip to knee )
        "rhipjoint", //rightUpLeg
        "lhipjoint", //leftUpLeg
        "lfemur", //left upper leg
        "ltibia", //left lower leg
        "rwrist", //right wrist
        "rhumerus", //right arm (shoulder to elbow)
        "rclavicle", //right shoulder
        "lclavicle", //left shoulder
        "lhumerus", //left arm ( shoulder to elbow)
        "lwrist", //left wrist
        "thorax", //chest
        "head",
    };

    private List<Transform> landmarkTransforms;

    private List<Vector3> landmarkLocations;

    public void Start () {
        landmarkLocations = new List<Vector3> ();
        landmarkTransforms = new List<Transform> ();
        landmarkTransforms.Add (rightFoot.transform);
        landmarkTransforms.Add (rightLeg.transform);
        landmarkTransforms.Add (rightUpLeg.transform);
        landmarkTransforms.Add (leftUpLeg.transform);
        landmarkTransforms.Add (leftLeg.transform);
        landmarkTransforms.Add (leftFoot.transform);
        landmarkTransforms.Add (rightHand.transform);
        landmarkTransforms.Add (rightArm.transform);
        landmarkTransforms.Add (rightShoulder.transform);
        landmarkTransforms.Add (leftShoulder.transform);
        landmarkTransforms.Add (leftArm.transform);
        landmarkTransforms.Add (leftHand.transform);
        landmarkTransforms.Add (spine1.transform);
        landmarkTransforms.Add (head.transform);
        for (int i = 0; i < 15; i++) {
            landmarkLocations.Add (Vector3.zero);
        }
    }

    public void Update () {
        for (int i = 0; i < landmarkTransforms.Count; i++) {
            landmarkLocations[i] = landmarkTransforms[i].position - transform.position;
            //landmarkLocations[i] = landmarkTransforms[i].position + dadbodAvatar.transform.position - transform.position;
        }
    }

    public JustWithJoints.Core.Pose getCurrentPose () {
        return new JustWithJoints.Core.Pose (0, landmarkLocations);
    }

}