using System;
using System.Collections;
using Mediapipe.Unity.PoseTracking;
using UnityEngine;
using UnityEngine.UI;

namespace Mediapipe.Unity {

    public class MediaPipeController : MonoBehaviour {
        private string TAG = "MediaPipeController";
        public bool usingWebcam;
        public RawImage webRTCImage;
        public RawImage webcamImage;

        public GameObject consoleObject;

        public MediaPipePoseTranslator poseTranslator;
        public PoseTrackingHandler poseTrackingHandler;

        private ImageSource.SourceType defaultImageSource;
        private InferenceMode preferableInferenceMode;

        public InferenceMode inferenceMode { get; private set; }
        public bool isFinished { get; private set; }

        private RawImage screen;
        public PoseTrackingGraph graphRunner;
        [SerializeField] TextureFramePool textureFramePool;
        Coroutine coroutine;
        private int modelComplexity = 1;
        private int poseLandmarkLength = 33;
        private bool smoothLandmarks = true;

        public RunningMode runningMode = RunningMode.Async;

        public GameObject imageSourceControllerObject;

        private void Awake () {
            imageSourceControllerObject.name = "ImageSourceController";
        }

        public void Start () {
            if (usingWebcam) {
                defaultImageSource = ImageSource.SourceType.Camera;
                Debug.Log ("using webcam. initiazing mediapipe now.");
                StartCoroutine (setup ());
            } else {
                defaultImageSource = ImageSource.SourceType.RawImage;
                Debug.Log ("using webrtc and sockets. waiting for a connection before intiialization.");
            }

        }

        public void Update () {
            if (Input.GetKeyDown (KeyCode.T)) {
                switch (usingWebcam) {
                    case true:
                        usingWebcam = false;
                        break;
                    case false:
                        usingWebcam = true;
                        break;
                }
            }

            if (Input.GetKeyDown (KeyCode.C)) {
                switch (consoleObject.activeSelf) {
                    case true:
                        consoleObject.SetActive (false);
                        break;
                    case false:
                        consoleObject.SetActive (true);
                        break;
                }
            }

        }

        // Start is called before the first frame update
        public IEnumerator setup () {
            yield return new WaitForEndOfFrame ();
            DecideInferenceMode ();
            if (inferenceMode == InferenceMode.GPU) {
                Logger.LogInfo (TAG, "Initializing GPU resources...");
                yield return GpuManager.Initialize ();
            }
            Logger.LogInfo (TAG, "Preparing ImageSource...");
            ImageSourceProvider.SwitchSource (defaultImageSource);
            isFinished = true;
            coroutine = StartCoroutine (Run ());
        }

        private void DecideInferenceMode () {
#if UNITY_EDITOR_OSX || UNITY_EDITOR_WIN
            if (preferableInferenceMode == InferenceMode.GPU) {
                Logger.LogWarning (TAG, "Current platform does not support GPU inference mode, so falling back to CPU mode");
            }
            inferenceMode = InferenceMode.CPU;
#else
            inferenceMode = preferableInferenceMode;
#endif
        }

        void OnApplicationQuit () {
            GpuManager.Shutdown ();
            Logger.SetLogger (null);
        }

        public IEnumerator Run () {
            var graphInitRequest = graphRunner.WaitForInit ();
            var imageSource = ImageSourceProvider.ImageSource;
            Logger.LogInfo (TAG, imageSource);
            if (imageSource != null) { } else {
                Logger.LogWarning (TAG, "No image source found");
                yield break;
            }

            yield return imageSource.Play ();

            if (!imageSource.isPrepared) {
                Logger.LogError (TAG, "Failed to start ImageSource, exiting...");
                yield break;
            }

            SetupScreen (imageSource);

            yield return graphInitRequest;
            if (graphInitRequest.isError) {
                Logger.LogError (TAG, graphInitRequest.error);
                yield break;
            }

            if (runningMode == RunningMode.Async) {
                graphRunner.OnPoseLandmarksOutput.AddListener (OnNormalizedPoseLandmarksOutput);
                graphRunner.OnPoseWorldLandmarksOutput.AddListener (OnPoseWorldLandmarksOutput);
                graphRunner.StartRunAsync (imageSource).AssertOk ();
            } else {
                graphRunner.StartRun (imageSource).AssertOk ();
            }

            // Use RGBA32 as the input format.
            // TODO: When using GpuBuffer, MediaPipe assumes that the input format is BGRA, so the following code must be fixed.
            // Debug.Log (imageSource.textureWidth + " , " + TextureFormat.RGBA32);
            textureFramePool.ResizeTexture (imageSource.textureWidth, imageSource.textureHeight, TextureFormat.RGBA32);

            while (true) {
                var textureFrameRequest = textureFramePool.WaitForNextTextureFrame ();
                yield return textureFrameRequest;
                var textureFrame = textureFrameRequest.result;
                ReadFromImageSource (textureFrame, runningMode, graphRunner.configType);
                graphRunner.AddTextureFrameToInputStream (textureFrame).AssertOk ();
                yield return new WaitForEndOfFrame ();
            }

        }

        private void SetupScreen (ImageSource imageSource) {
            if (screen == null) {
                var sourceTexture = ImageSourceProvider.ImageSource.GetCurrentTexture ();
                var textureType = sourceTexture.GetType ();
                screen = webcamImage; //TODO - investigate why this is needed. 
                screen.rectTransform.localEulerAngles = imageSource.rotation.Reverse ().GetEulerAngles ();
                if (imageSource.isVerticallyFlipped) {
                    screen.uvRect = new UnityEngine.Rect (0, 1, 1, -1);
                }
                screen.texture = imageSource.GetCurrentTexture ();
                if (!usingWebcam) {
                    screen.enabled = false;
                } else {
                    webRTCImage.enabled = false;
                }
            }
        }

        private void ReadFromImageSource (TextureFrame textureFrame, RunningMode runningMode, GraphRunner.ConfigType configType) {
            var sourceTexture = ImageSourceProvider.ImageSource.GetCurrentTexture ();
            //latency is high on GPU for some reason. 
            var textureType = sourceTexture.GetType ();

            if (webcamImage != null && textureType == typeof (WebCamTexture)) {
                textureFrame.ReadTextureFromOnCPU ((WebCamTexture) webcamImage.texture);
            } else if (webRTCImage != null) {
                textureFrame.ReadTextureFromOnCPU (webRTCImage.texture);
            }
        }

        void OnNormalizedPoseLandmarksOutput (NormalizedLandmarkList poseLandmarks) {
            //customNormalizedLandmarkOutput (poseLandmarks);
        }

        void OnPoseWorldLandmarksOutput (LandmarkList poseWorldLandmarks) {
            //using these for justjoints.
            customLandmarkOutput (poseWorldLandmarks);
        }

        void customLandmarkOutput (LandmarkList poseLandmarks) {
            if (poseLandmarks != null) {
                for (var i = 0; i < poseLandmarkLength; i++) {
                    poseTranslator.landmarkHandler (i, poseLandmarks.Landmark[i]);
                    poseTrackingHandler.landmarkHandler (i, poseLandmarks.Landmark[i]);
                }
                poseTranslator.landmarkHandler (-1, null); //for chest calculation

            } else {
                poseTranslator.trackingLostHandler ();
            }
        }

        void customNormalizedLandmarkOutput (NormalizedLandmarkList poseLandmarks) {
            if (poseLandmarks != null) {
                for (var i = 0; i < poseLandmarkLength; i++) {
                    poseTrackingHandler.normalizedLandmarkHandler (i, poseLandmarks.Landmark[i]);
                }
            }
            //poseTranslator.normalizedLandmarkHandler (i, poseLandmarks.Landmark[i]);
            //poseTranslator.normalizedLandmarkHandler (-1, null); //for chest calculation
        }
    }
}