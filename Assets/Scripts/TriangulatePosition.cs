using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangulatePosition : MonoBehaviour {
    public GameObject p1_go;
    public GameObject p2_go;
    public GameObject p3_go;
    public GameObject normalIndicator;

    public Vector3 postRotationCorrection;

    private Vector3 p1;
    private Vector3 p2;
    private Vector3 p3;

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        updatePositions ();
        triangulatePosition ();
    }

    void updatePositions () {
        p1 = p1_go.transform.position;
        p2 = p2_go.transform.position;
        p3 = p3_go.transform.position;
    }

    void triangulatePosition () {
        Vector3 vec1 = p1 - p2;
        Vector3 vec2 = p1 - p3;
        Vector3 normalVec = Vector3.Cross (vec1, vec2);
        normalIndicator.transform.rotation = Quaternion.LookRotation (normalVec, vec1) * Quaternion.Euler (postRotationCorrection);
    }
}