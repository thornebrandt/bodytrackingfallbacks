using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoseTrackingHandler : MonoBehaviour {
    public GameObject indicatorPrefab;
    public GameObject headIndicator;
    public GameObject neckJoint;
    public GameObject headJoint;
    public GameObject root;
    public Vector3 headRotationPreFix;
    public Vector3 headRotationPostFix;
    public Vector3 headRotationPostFix2;
    public bool normalized;
    public int poseLandmarkLength = 33;
    public float normalizedBodyScale = 5;
    public float indicatorScale = 0.1f;

    private List<GameObject> indicators;
    private Vector3[] landmarkLocations;
    private float[] landmarkPresence;
    private float[] landmarkVisibility;
    private bool trackingStarted;

    private Vector3 headP1; //nose
    private Vector3 headP2; //left ear
    private Vector3 headP3; //right ear
    private Quaternion headRotation;

    void Start () {
        createPool ();
        landmarkLocations = new Vector3[poseLandmarkLength];
        landmarkPresence = new float[poseLandmarkLength];
        landmarkVisibility = new float[poseLandmarkLength];
        headRotation = Quaternion.identity;
    }

    void Update () {
        if (trackingStarted) {
            for (int i = 0; i < landmarkLocations.Length; i++) {
                indicators[i].transform.localPosition = landmarkLocations[i];
                indicators[i].transform.localScale = ((Vector3.one * landmarkVisibility[i]) + new Vector3 (0.1f, 0.1f, 0.1f)) * indicatorScale;
                collectHeadPositions (i, landmarkLocations[i]);
            }
            headRotation = calculateHeadRotation ();
            //Debug.Log (headRotation.eulerAngles);

            headIndicator.transform.rotation = headRotation;

            Vector3 headRotationEuler = headRotation.eulerAngles;
            //Quaternion manualTranslation = Quaternion.Euler (-headRotationEuler.y - 90, -headRotationEuler.x, -headRotationEuler.z + 90);
            //neckJoint.transform.localRotation = root.transform.rotation * manualTranslation;

            Quaternion manualTranslation = Quaternion.Euler (headRotationEuler.x, headRotationEuler.y, headRotationEuler.z);
            neckJoint.transform.rotation = root.transform.rotation * manualTranslation * Quaternion.Euler (headRotationPostFix2);

            //neckJoint.transform.localRotation = Quaternion.Euler (headRotationPostFix);
        }
    }

    private Quaternion calculateHeadRotation () {
        //make sure the points are confident, otherwise, quaternion identity.

        Vector3 vec1 = headP2 - headP1;
        Vector3 vec2 = headP2 - headP3;
        Vector3 normalVec = Vector3.Cross (vec1, vec2);
        return Quaternion.Euler (headRotationPreFix) * Quaternion.LookRotation (normalVec, vec1) * Quaternion.Euler (headRotationPostFix);
    }

    private void collectHeadPositions (int i, Vector3 _position) {
        switch (i) {
            case 0:
                headP1 = _position;
                //headIndicator.transform.localPosition = _position;
                break;
            case 6:
                headP2 = _position;
                break;
            case 3:
                headP3 = _position;
                break;
            default:
                break;
        }
    }

    void createPool () {
        indicators = new List<GameObject> ();
        for (int i = 0; i < poseLandmarkLength; i++) {
            GameObject indicator = Instantiate (indicatorPrefab) as GameObject;
            indicator.transform.SetParent (transform); //set parent to this object for now. 
            indicators.Add (indicator);
        }
    }

    public void normalizedLandmarkHandler (int i, Mediapipe.NormalizedLandmark normalizedLandmark) {
        if (normalized) {
            trackingStarted = true;
            landmarkLocations[i] = new Vector3 (
                normalizedLandmark.X,
                normalizedLandmark.Y,
                normalizedLandmark.Z
            ) * normalizedBodyScale;
            landmarkPresence[i] = normalizedLandmark.Presence;
            landmarkVisibility[i] = normalizedLandmark.Visibility;
        }
    }

    public void landmarkHandler (int i, Mediapipe.Landmark landmark) {
        if (!normalized) {
            trackingStarted = true;
            landmarkLocations[i] = new Vector3 (
                landmark.X,
                landmark.Y,
                landmark.Z
            );
            landmarkPresence[i] = landmark.Presence;
            landmarkVisibility[i] = landmark.Visibility;
        }
    }
}