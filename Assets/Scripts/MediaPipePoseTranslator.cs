using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JustWithJoints;
using UnityEngine;

public class MediaPipePoseTranslator : MonoBehaviour {

    [HideInInspector]
    public JustWithJoints.Core.Pose pose;
    private float bottomJointSmoothing = 3;
    public float confidenceSmoothing = 8;
    public float landmarkConfidenceThreshold = 0.5f;
    [HideInInspector]
    public float legsConfidence = 0;
    [HideInInspector]
    public float torsoConfidence = 0; //smoothed
    private float currentTorsoConfidence = 0; //absolute. 
    [HideInInspector]
    public bool torsoOnlyMode;
    [HideInInspector]
    public bool lostTracking;

    private string[] boneNames = {
        "rtibia", //right lower leg  ( shin ) 
        "rfemur", //right upper leg ( hip to knee )
        "rhipjoint", //right hip joint
        "lhipjoint", //right hip
        "lfemur", //left upper leg
        "ltibia", //left lower leg
        "rwrist", //right wrist
        "rhumerus", //right arm (shoulder to elbow)
        "rclavicle", //right shoulder
        "lclavicle", //left shoulder
        "lhumerus", //left arm ( shoulder to elbow)
        "lwrist", //left wrist
        "thorax", //chest
        "head",
    };

    private int[] mediaPipeLandmarkIDs = {
        27, //0 rtibia
        25, //1 rfemur
        23, //2 rhip
        24, //3 lhip,
        26, //4 lfemur
        28, //5 ltibia,
        15, //6 rwrist,
        13, //7 rhumerus,
        11, //8 rclavicle
        12, //9 lclavicle,
        14, //10 lhumerus,
        16, //11 lwrist,
        -1, //12  ----  lerping bertween NA chest needs to be calculated  by 11 and 24, or  [3] and [8]
        0, //13 head
    };

    private Vector3[] landmarkLocations;
    private Vector3[] heightAdjustedLandmarkLocations;
    private float[] landmarkVisibility;
    private float[] landmarkPresence;
    private Vector3 currentMinLandmark; //for lerping and getting bottom value. 

    void Start () {
        landmarkLocations = new Vector3[14];
        heightAdjustedLandmarkLocations = new Vector3[14];
        landmarkVisibility = new float[14];
        landmarkPresence = new float[14];
    }

    public void landmarkHandler (int _ID, Mediapipe.Landmark landmark) {
        for (int i = 0; i < mediaPipeLandmarkIDs.Length; i++) {
            if (mediaPipeLandmarkIDs[i] == _ID) {
                if (_ID == -1) {
                    //chest does not exist from mediapipe, calculating last with special -1. 

                    Vector3 lhip = landmarkLocations[3];
                    Vector3 rShoulder = landmarkLocations[8];
                    Vector3 chestLocation = Vector3.Lerp (lhip, rShoulder, 0.5f);
                    landmarkLocations[i] = chestLocation;
                } else {
                    landmarkLocations[i] = new Vector3 (-landmark.X, -landmark.Y,
                        landmark.Z
                    );
                    landmarkVisibility[i] = landmark.Visibility;
                    landmarkPresence[i] = landmark.Presence;
                }
            }
        }
    }

    private void getConfidence () {
        torsoOnlyMode = false;
        lostTracking = false;

        float lowerSum = 0; //legs
        float upperSum = 0;
        int lowerAmount = 0;
        int upperAmount = 0;

        for (int i = 0; i < landmarkVisibility.Length; i++) {
            if (i < 6) {
                lowerSum += landmarkVisibility[i];
                lowerAmount++;
            }
            if (i == 6 || i == 7 || i == 10 || i == 11) {
                //checking arms and hands 
                upperSum += landmarkVisibility[i];
                upperAmount++;
            }
        }

        //printPresence ();

        legsConfidence = lowerSum / lowerAmount;
        currentTorsoConfidence = upperSum / upperAmount;
        torsoConfidence = Mathf.Lerp (torsoConfidence, currentTorsoConfidence, Time.deltaTime * confidenceSmoothing);
        if (legsConfidence < landmarkConfidenceThreshold) {
            torsoOnlyMode = true;
        }
        if (torsoConfidence < landmarkConfidenceThreshold) {
            lostTracking = true;
        } else {
            //printVisibility ();
        }
    }

    private void printVisibility () {
        string s = "visibility ";
        for (int i = 6; i < landmarkVisibility.Length; i++) {
            s += i + ": " + landmarkVisibility[i] + " , ";
        }
        Debug.Log (s);
    }

    private void printPresence () {
        string s = "presence ";
        for (int i = 6; i < landmarkVisibility.Length; i++) {
            s += i + ": " + landmarkVisibility[i] + " , ";
        }
        Debug.Log (s);
    }

    public void trackingLostHandler () {
        //set all visibility to zero.
        lostTracking = true;
        torsoConfidence = 0;
        for (int i = 0; i < landmarkVisibility.Length; i++) {
            landmarkVisibility[i] = 0;
        }
    }

    public JustWithJoints.Core.Pose getCurrentPose () {

        if (landmarkLocations[13] != null) {
            getConfidence ();

            heightAdjustedLandmarkLocations = getHeightAdjustedLandmarks (landmarkLocations);
            //return new JustWithJoints.Core.Pose (0, landmarkLocations.ToList ()); original
            return new JustWithJoints.Core.Pose (0, heightAdjustedLandmarkLocations.ToList ());
        }
        return pose;
    }

    private Vector3[] getHeightAdjustedLandmarks (Vector3[] landmarkLocations) {
        //
        Vector3 betweenFeet = Vector3.Lerp (landmarkLocations[0], landmarkLocations[5], 0.5f);
        Vector3 centerHips = Vector3.Lerp (landmarkLocations[2], landmarkLocations[3], 0.5f);

        float goalFloorY = betweenFeet.y;

        Vector3 goalFloor = new Vector3 (0, goalFloorY, 0);

        if (lostTracking || torsoOnlyMode) {
            goalFloor = Vector3.zero;
        }

        //debug, do positional tracking in post. 
        goalFloor = Vector3.zero;
        currentMinLandmark = goalFloor;

        //currentMinLandmark = Vector3.Lerp (currentMinLandmark, goalFloor, Time.deltaTime * bottomJointSmoothing);

        for (int j = 0; j < heightAdjustedLandmarkLocations.Length; j++) {
            heightAdjustedLandmarkLocations[j] = landmarkLocations[j] - currentMinLandmark;
        }

        // if (goalFloor != Vector3.zero) {
        //     Vector3 adjustedCenterHips = Vector3.Lerp (heightAdjustedLandmarkLocations[2], heightAdjustedLandmarkLocations[3], 0.5f);
        //     Debug.Log ("goalFloor: " + goalFloor + " centerHips: " + centerHips + " becomes: " + adjustedCenterHips);
        // }

        return heightAdjustedLandmarkLocations;
    }
}